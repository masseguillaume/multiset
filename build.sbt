name := "multisets"

version := "0.1"

organization := "com.sidewayscoding"

scalaVersion := "2.10.0"

resolvers ++= Seq(
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases"
)

scalacOptions ++= Seq(
  "-deprecation"
)

libraryDependencies ++= Seq( 
  "org.specs2" 		% "specs2_2.10" 	% "1.14" 	% "test",
  "org.scalacheck" 	% "scalacheck_2.10" % "1.10.0" 	% "test"
)

initialCommands := """
  import com.sidewayscoding._
  import com.sidewayscoding.immutable._"""
